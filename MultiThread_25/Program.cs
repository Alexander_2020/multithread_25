﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThread_25
{
    class Program
    {
        static async Task Main(string[] args)
        {
            TimeSpan timeSpan;
            var collectionInts = Enumerable.Repeat(1, 100_000_000).ToList();
        
            GetSum("Обычное", (() => collectionInts.Sum()));
         
            var portions = new List<List<int>>();
            var partLenght = collectionInts.Count / 4;
            
            for (var i = 0; i < 4; i++)
            {
                portions.Add(collectionInts.Skip(i*partLenght).Take(partLenght).ToList());
            }
            
            await GetSum("Параллельное Thread,", (async () =>
            {
                var threads = new List<Task<int>>();
        
                for (var i = 0; i < 4; i++)
                {
                    int index = i;
                    var thread = new Task<int>(() => portions[index].Sum());
        
                    threads.Add(thread);
                    thread.Start();
                }
        
                return (await Task.WhenAll(threads)).Sum();
            }));
            
            GetSum("Параллельное с помощью LINQ", (() => collectionInts.AsParallel().Sum()));
            
            Console.ReadKey();
        }
        
        private static void GetSum(string descr, Func<int> f)
        {
            //DateTime startTime = DateTime.Now;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            int sum = f.Invoke();
            stopwatch.Stop();
            Console.WriteLine($"{descr}: сумма = {sum} to {stopwatch.ElapsedMilliseconds} мс.");
        }       
        
        private static async Task GetSum(string descr, Func<Task<int>> f)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            int sum = await f.Invoke();
            stopwatch.Stop();
            Console.WriteLine($"{descr}: сумма = {sum} to {stopwatch.ElapsedMilliseconds} мс.");
        }
    }
}